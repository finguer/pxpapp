package com.example.lenovo.pxp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CamaraFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class CamaraFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public CamaraFragment() {
        // Required empty public constructor
    }

    Button buttonCamara;
    private Uri fileUri;


    Button btpic, btnup;
    String picturePath;
    Uri selectedImage;
    Bitmap photo;
    String ba1;
    View viewRoot;


    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1888;
    Button button;
    ImageView imageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_camara, container, false);

        viewRoot = view;
        buttonCamara = (Button)view.findViewById(R.id.buttonCamara);
        buttonCamara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getContext().getApplicationContext().getPackageManager().hasSystemFeature(
                        PackageManager.FEATURE_CAMERA)) {

                    //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //startActivityForResult(intent,
                            //CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);



                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

                    // start the image capture Intent
                    startActivityForResult(intent, 100);

                    // start the image capture Intent

                } else {
                    Toast.makeText(getContext(), "error de la camara", Toast.LENGTH_LONG).show();
                }

            }
        });


        return view;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }




    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("ima requestCode", String.valueOf(requestCode));
        Log.d("ima resultCode", String.valueOf(resultCode));
       // if (requestCode == 100 && resultCode == Activity.RESULT_OK) {


            selectedImage = data.getData();
            photo = (Bitmap) data.getExtras().get("data");
        Log.d("photo", String.valueOf(photo));

            // Cursor to get image uri to display

            /*String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContext().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();*/

            Bitmap photo = (Bitmap) data.getExtras().get("data");

             ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.PNG, 1, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();

            String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

            Log.d("encoded", encoded);
            ImageView imageView = (ImageView)viewRoot.findViewById(R.id.imageViewCamara);
            imageView.setImageBitmap(photo);
      //  }
    }



}
