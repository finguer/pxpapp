package com.example.lenovo.pxp.modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by dyd on 19/03/2016.
 */
public class DataBaseTarifas {

    public static final String CREATE_TABLE_TARIFAS = "CREATE TABLE tarifas (_id INTEGER PRIMARY KEY AUTOINCREMENT,categoria TEXT, rango TEXT, tarifa TEXT,servicio TEXT, min NUMERIC,max NUMERIC,costo NUMERIC,costo_variable NUMERIC,exento NUMERIC, porcentaje NUMERIC, id_tarifa_agua_acl INTEGER,id_categoria INTEGER,id_tipo_tarifa INTEGER,id_rango_tarifa INTEGER)";

    private DbHelper helper;
    private SQLiteDatabase db;



    public DataBaseTarifas(Context context){
        helper = new DbHelper(context);
        db = helper.getWritableDatabase();
    }
    public void cerrarDb(){
        db.close();
    }
    public void abriDb(){
        db.isOpen();
    }

    public ContentValues generarContentValues(String categoria, String rango , String tarifa,String servicio, Float min,Float max,Float costo,Float costo_variable,Float porcentaje,Integer id_tarifa_agua_acl,Integer id_categoria,Integer id_tipo_tarifa,Integer id_rango_tarifa,Float exento){

        ContentValues valores = new ContentValues();
        valores.put("categoria",categoria);
        valores.put("rango",rango);
        valores.put("tarifa",tarifa);
        valores.put("servicio",servicio);
        valores.put("min",min);
        valores.put("max",max);
        valores.put("costo",costo);
        valores.put("costo_variable",costo_variable);
        valores.put("porcentaje",porcentaje);
        valores.put("id_tarifa_agua_acl",id_tarifa_agua_acl);
        valores.put("id_categoria",id_categoria);
        valores.put("id_tipo_tarifa",id_tipo_tarifa);
        valores.put("id_rango_tarifa",id_rango_tarifa);
        valores.put("exento",exento);
        return valores;
    }

    public void insertar(String categoria, String rango , String tarifa,String servicio, Float min,Float max,Float costo,Float costo_variable,Float porcentaje,Integer id_tarifa_agua_acl,Integer id_categoria,Integer id_tipo_tarifa,Integer id_rango_tarifa,Float exento){

        db.insert( "tarifas",null,generarContentValues(categoria, rango , tarifa,servicio, min,max,costo,costo_variable,porcentaje,id_tarifa_agua_acl,id_categoria,id_tipo_tarifa,id_rango_tarifa,exento) );

        db.close();

    }

    public Cursor listarTarifas(){

        /*String[] columnas = new String []{"id", "categoria", "rango", "tarifa","servicio", "min","max", "costo", "costo_variable", "porcentaje", "id_tarifa_agua_acl", "id_categoria", "id_tipo_tarifa", "id_rango_tarifa"};*/
        String[] columnas = new String []{"_id", "categoria","rango"};
        return db.query("tarifas",columnas,null,null,null,null,null);


    }
}
