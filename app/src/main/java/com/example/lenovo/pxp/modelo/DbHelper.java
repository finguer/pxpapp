package com.example.lenovo.pxp.modelo;

import android.content.Context;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by favio figueroa on 21-Jan-16.
 */
public class DbHelper extends SQLiteOpenHelper {


    private static final String DB_NAME = "pxp";
    private static final int DB_SCHEME_VERSION =1;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_SCHEME_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(DataBaseHostUsuario.CREATE_TABLE);
        db.execSQL(DataBaseTarifas.CREATE_TABLE_TARIFAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
