package com.example.lenovo.pxp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.lenovo.pxp.control.ControlHostUsuario;
import com.example.lenovo.pxp.pxp.PxpService;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link peril.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class peril extends Fragment {

    private OnFragmentInteractionListener mListener;

    public peril() {
        // Required empty public constructor
    }


    private PxpService pxpService = new PxpService();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        String strtext = getArguments().getString("pwd");
        String usuario = getArguments().getString("usuario");
        Log.d("strtext",strtext);
        Log.d("usuario",usuario);


        final View view = inflater.inflate(R.layout.fragment_peril, container, false);
        try {

            //Log.d("pwd de sqlite",pwd);


            pxpService.setUsuario(usuario);
            pxpService.setPwd(strtext);
            pxpService.dominioDeServicio("prueba.kplian.com", "seguridad", "Usuario", "listarUsuario");
            RequestBody formBody = new FormBody.Builder()
                    .add("start", "0")
                    .add("limit", "1")
                    .add("sort", "id_persona")
                    .add("dir", "desc")
                    .build();
            pxpService.setParametros(formBody);
            Call call = pxpService.iniciar(getContext(),false);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("error", String.valueOf(call));
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        //Log.e(TAG,response.body().string());
                        String jsonData = response.body().string();
                        //Log.d("json", jsonData);
                        if (response.isSuccessful()) {
                            Log.d("json", jsonData);

                        } else {
                            pxpService.setMensaje(pxpService.ExisteError(jsonData));

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getContext(), pxpService.getMensaje(), Toast.LENGTH_LONG).show();
                                }
                            });


                        }

                    } catch (IOException e) {
                        Log.e("", "ERROR", e);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
