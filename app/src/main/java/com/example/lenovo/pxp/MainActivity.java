package com.example.lenovo.pxp;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lenovo.pxp.control.ControlHostUsuario;
import com.example.lenovo.pxp.modelo.DataBaseHostUsuario;
import com.example.lenovo.pxp.modelo.DbHelper;
import com.example.lenovo.pxp.pxp.PxpService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {



    private PxpService pxpService = new PxpService();
    public Context contexGlobal;

    Button ButtonLogin;

    EditText editTextHostIp;
    EditText editTextUsuario;
    EditText editTextPassword;

    private ControlHostUsuario controlHostUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        startMainMenu();
        //crea las base de datos
        DbHelper helper = new DbHelper(this);
        SQLiteDatabase db = helper.getWritableDatabase();


        ButtonLogin = (Button)findViewById(R.id.buttonLogin);
        ButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("fa","llega");

                editTextHostIp = (EditText)findViewById(R.id.editTextHostIp);
                editTextUsuario = (EditText)findViewById(R.id.editTextUsuario);
                editTextPassword = (EditText)findViewById(R.id.editTextPassword);


                String host = editTextHostIp.getText().toString();
               String usuario =  editTextUsuario.getText().toString();
               String pwd = editTextPassword.getText().toString();

                boolean validado=true;
                if(host.equals("")){
                    validado=false;
                    Toast.makeText(contexGlobal,"Host no debe estar vacio", Toast.LENGTH_LONG).show();
                }
                if(usuario.equals("")){
                    validado=false;
                    Toast.makeText(contexGlobal,"usuario no debe estar vacio", Toast.LENGTH_LONG).show();
                }
                if(pwd.equals("")){
                    validado=false;
                    Toast.makeText(contexGlobal,"pasword no debe estar vacio", Toast.LENGTH_LONG).show();
                }


                if(validado){
                    session(host, usuario, pwd);
                    //startMainMenu();
                }

            }
        });


        contexGlobal = this;




    }

    private void startMainMenu(){
        Intent intent = new Intent(this,MainMenuActivity.class);

        startActivity(intent);

    }

    private void session(final String host, final String usuario, final String pwd){

        try {
            pxpService.setUsuario(usuario);
            pxpService.setPwd(pwd);
            pxpService.dominioDeServicio(host, "seguridad", "Usuario", "listarUsuario");
            RequestBody formBody = new FormBody.Builder()
                    .add("start", "0")
                    .add("limit", "1")
                    .add("sort", "id_usuario")
                    .add("dir", "desc")
                    .add("filter", "[{\"type\":\"string\",\"value\":\""+usuario+"\",\"field\":\"cuenta\"}]")
                    .build();
            pxpService.setParametros(formBody);

            Call call = pxpService.iniciar(contexGlobal,true);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("error", String.valueOf(call));
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        //Log.e(TAG,response.body().string());
                        String jsonData = response.body().string();
                        //Log.d("json", jsonData);
                        if (response.isSuccessful()) {
                            Log.d("json", jsonData);
                            
                            controlHostUsuario = getCurrentDetails(jsonData,host,usuario,pwd);

                           

                        } else {
                            pxpService.setMensaje(pxpService.ExisteError(jsonData));


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(contexGlobal, pxpService.getMensaje(), Toast.LENGTH_LONG).show();
                                }
                            });

                        }

                    } catch (IOException e) {
                        Log.e("", "ERROR", e);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                private ControlHostUsuario getCurrentDetails(String jsonData,String host,String usuario,String pwd) throws JSONException {

                    JSONObject json_pxp = new JSONObject(jsonData);
                    JSONArray datos = json_pxp.getJSONArray("datos");

                    ControlHostUsuario currentHostUsuario = new ControlHostUsuario();


                    for (int i = 0; i < datos.length(); i++) {

                        Address current = new Address(Locale.getDefault());
                        JSONObject item = datos.getJSONObject(i);

                        currentHostUsuario.setEstado("activo");
                        currentHostUsuario.setHost(host);
                        currentHostUsuario.setUsuario(item.getString("cuenta"));
                        currentHostUsuario.setPwd(item.getString("contrasena"));
                        currentHostUsuario.setDesc_person(item.getString("desc_person"));
                        currentHostUsuario.insertarHostUsuario(contexGlobal);
                        Log.d("estado",currentHostUsuario.getEstado());
                        Log.d("estado", currentHostUsuario.getHost());

                    }

                    startMainMenu();
                    return new ControlHostUsuario();
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }

    }




}
